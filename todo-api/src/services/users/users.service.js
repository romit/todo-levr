// Initializes the `users` service on path `/users`
const createService = require('feathers-mongoose');
const createModel = require('../../models/users.model');
const hooks = require('./users.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'users',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  // app.use('/users', createService(options));

  const users = createService(options);
  users.docs = {
    get: {
      summary: "Retrieves a user by id",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "path",
          "name": "_id",
          "required": true,
          "type": "string"
        }
      ]
    },
    update: {
      summary: "Updates a user by id",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "path",
          "name": "_id",
          "required": true,
          "type": "string"
        },
        {
          "in": "body",
          "name": "body",
          "required": true,
          "schema":{"$ref":"#/definitions/User"}
        }
      ]
    },
    patch: {
      summary: "Updates a user by id",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "path",
          "name": "_id",
          "required": true,
          "type": "string"
        },
        {
          "in": "body",
          "name": "body",
          "required": true,
          "schema":{"$ref":"#/definitions/User"}
        }
      ]
    },
    remove: {
      summary: "Deletes a user by id",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "path",
          "name": "_id",
          "required": true,
          "type": "string"
        }
      ]
    },
    create: {
      summary: "Creates a new user",
      parameters: [
        {
          "in": "body",
          "name": "body",
          "required": true,
          "schema":{"$ref":"#/definitions/User"}
        }
      ]
    },
    find: {
      summary: "Retrieves all users",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          description: 'Number of results to return',
          in: 'query',
          name: '$limit',
          type: 'integer'
        }
      ]
    },
    definitions: {
      "User": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "email": {
            "type": "string"
          },
          "password": {
            "type": "string"
          }
        },
        "xml": {
          "name": "User"
        }
      }
    }
  }

  app.use('/users', users);

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('users');

  service.hooks(hooks);
};
