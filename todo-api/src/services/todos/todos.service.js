// Initializes the `todos` service on path `/todos`
const createService = require('feathers-mongoose');
const createModel = require('../../models/todos.model');
const hooks = require('./todos.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'todos',
    Model,
    paginate
  };

  // Initialize our service with any options it requires

  const todos = createService(options);
  todos.docs = {
    //overwrite things here.
    //if we want to add a mongoose style $search hook to find, we can write this:
    get: {
      summary: "Retrieves a todo by id",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "path",
          "name": "_id",
          "required": true,
          "type": "string"
        }
      ]
    },
    remove: {
      summary: "Deletes a todo by id",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "path",
          "name": "_id",
          "required": true,
          "type": "string"
        }
      ]
    },
    update: {
      summary: "Updates a todo by id",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "path",
          "name": "_id",
          "required": true,
          "type": "string"
        },
        {
          "in": "body",
          "name": "body",
          "required": true,
          "schema":{"$ref":"#/definitions/Todo"}
        }
      ]
    },
    patch: {
      summary: "Updates a todo by id",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "path",
          "name": "_id",
          "required": true,
          "type": "string"
        },
        {
          "in": "body",
          "name": "body",
          "required": true,
          "schema":{"$ref":"#/definitions/Todo"}
        }
      ]
    },
    create: {
      summary: "Add a new todo to the user todos list",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          "in": "body",
          "name": "body",
          "required": true,
          "schema":{"$ref":"#/definitions/Todo"}
        }
      ]
    },
    find: {
      summary: "Retrieves all todos crearted by a user",
      parameters: [
        {
          "in": "header",
          "name": "authorization",
          "required": true,
          "type": "string"
        },
        {
          description: 'Number of results to return',
          in: 'query',
          name: '$limit',
          type: 'integer'
        }
      ]
    },
    definitions: {
      "Todo": {
        "type": "object",
        "properties": {
          "text": {
            "type": "string"
          },
          "completed": {
            "type": "boolean"
          }
        },
        "xml": {
          "name": "Todo"
        }
      }
    }
  }
  app.use('/todos', todos);

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('todos');

  service.hooks(hooks);
};
