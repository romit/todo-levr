const { authenticate } = require('@feathersjs/authentication').hooks;
const authHooks = require('feathers-authentication-hooks');
const processTodo = require('../../hooks/process-todo');
const filterFindAllTodos = require('../../hooks/filter-find-all-todos');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [ authHooks.queryWithCurrentUser() ],
    get: [ authHooks.restrictToOwner() ],
    create: [ processTodo() ],
    update: [ authHooks.restrictToOwner() ],
    patch: [ authHooks.restrictToOwner() ],
    remove: [ authHooks.restrictToOwner() ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
