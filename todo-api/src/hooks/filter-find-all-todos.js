// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
  	context.params.query = { userId : context.params.user._id, $sort: {createdAt: -1} };
    return context;
  };
};
