const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const filterFindAllTodos = require('../../src/hooks/filter-find-all-todos');

describe('\'filter-find-all-todos\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: filterFindAllTodos()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
