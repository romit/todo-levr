function pluralize (count, word) {
	return count === 1 ? word : word + 's';
}

function store (namespace, data) {
	if (data) {
		return localStorage.setItem(namespace, JSON.stringify(data));
	}

	var store = localStorage.getItem(namespace);
	return (store && JSON.parse(store)) || [];
}

function clearStore(namespace){
	localStorage.clear(namespace);
}

function extend () {
	var newObj = {};
	for (var i = 0; i < arguments.length; i++) {
		var obj = arguments[i];
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				newObj[key] = obj[key];
			}
		}
	}
	return newObj;
}

module.exports = {
  pluralize: pluralize,
  store: store,
  extend: extend,
  clearStore: clearStore
};