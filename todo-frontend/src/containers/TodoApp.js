import React, { Component } from "react";
import TodoList from './TodoList';
import TodoForm from './TodoForm';
import axios from 'axios';
import "./TodoApp.css";

var Utils = require('../utils');

var instance;

export default class TodoApp extends Component{

	constructor(props){
		super(props);

		this.state = {
			data: []
		};

		this.generateId = this.generateId.bind(this);
		this.handleNodeRemoval = this.handleNodeRemoval.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleToggleComplete = this.handleToggleComplete.bind(this);
	}

	componentDidMount(){

		if(Utils.store('user').length!==0){
			instance = axios.create({
			  baseURL: 'http://localhost:3030',
			  timeout: 1000,
			  headers: {'Content-Type': 'application/json', 'accept': 'application/json', 'authorization': Utils.store('user')}
			});

		    instance.get('/todos')
		    .then(res => {
		        console.log(res.data.data);
		        this.setState({data: res.data.data});
		    })
		    .catch(function (error) {
		      alert(error.message);
		    });
		}
	}

	getInitialState(){
		return this.state.data;
	}

	generateId () {
		return Math.floor(Math.random()*90000) + 10000;
	}

	handleNodeRemoval (nodeId) {

		instance.delete('/todos/'+nodeId)
		.then(res => {
			var data = this.state.data;
			data = data.filter(function (el) {
				return el._id !== nodeId;
			});
			this.setState({data});
		})
		.catch(function (error) {
	      alert(error.message);
	    });

		return;
	}

	handleSubmit (task) {

		var newTask = {
			text: task,
			completed: false
		};

		instance.post('/todos', newTask)
		.then(res => {
			console.log(res.data);
			var data = this.state.data;
			var newData = [res.data];
			this.setState({data: newData.concat(data)});
		})
		.catch(function (error) {
	      alert(error.message);
	    });
	}

	handleToggleComplete (nodeId) {
		
		var updatedTask;
		var data = this.state.data;
		for (var i in data) {
			if (data[i]._id === nodeId) {
				data[i].completed = data[i].completed === true ? false : true;
				updatedTask = data[i];
				break;
			}
		}

		instance.put('/todos/'+nodeId, updatedTask)
		.then(res => {
			this.setState({data});
		})
		.catch( error => {
			alert(error.message);
		})
		return;
	}

	render() {
		return (
			<div className='row'>
			<div className="col-md-8 col-md-offset-2">
				<h1 className="text-center">todos</h1>
				<TodoForm onTaskSubmit={this.handleSubmit} />
				<TodoList data={this.state.data} removeNode={this.handleNodeRemoval} toggleComplete={this.handleToggleComplete} />
			</div>
			</div>
		);
	}
}