import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import "./Login.css";
import axios from 'axios';

var Utils = require('../utils.js');

export default class Login extends Component {
  constructor(props) {
    super(props);

    if(this.props.isAuthenticated){
      this.props.history.push('/');
    }

    this.state = {
      isLoading: false,
      email: "",
      password: ""
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    const userModel = {
      strategy: "local",
      email: this.state.email,
      password: this.state.password
    };

    var instance = axios.create({
      baseURL: 'http://localhost:3030',
      timeout: 1000,
      headers: {'Content-Type': 'application/json', 'accept': 'application/json'}
    });

    this.setState({ isLoading: true });

    instance.post(`/auth`, userModel)
      .then(res => {
        console.log(res);
        this.props.userHasAuthenticated(true);
        Utils.store('user', res.data.accessToken);
        this.props.history.push("/");    
    })
    .catch(function (error) {
      alert(error.message);
    });

    this.setState({ isLoading: false });
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Login"
            loadingText="Logging in…"
          />
        </form>
      </div>
    );
  }
}