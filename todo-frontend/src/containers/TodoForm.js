import React, { Component } from "react";

export default class TodoForm extends Component{

	constructor(props){
		super(props);
		this.state = {
			value: ''
		};

		this.doSubmit = this.doSubmit.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
	    this.setState({value: event.target.value});
	  }

	doSubmit (e) {
		e.preventDefault();
		var task = this.state.value;
		if (!task) {
			return;
		}
		this.props.onTaskSubmit(task);
		this.setState({value:''});
		return;
	}

	render() {
		return (
			<div className="commentForm vert-offset-top-2">
				<hr />
				<div className="clearfix">
					<form className="todoForm form-horizontal" onSubmit={this.doSubmit}>
						<div className="form-group">
							<div className="col-md-12">
								<input type="text" value={this.state.value} id="task" ref="task" className="form-control new-task-input" placeholder="What do you need to do?" onChange={this.handleChange}/>
							</div>
						</div>
					</form>
				</div>
			</div>
		);
	}
}