import React, { Component } from "react";

export default class TodoItem extends Component{

	constructor(props){
		super(props);

		this.removeNode = this.removeNode.bind(this);
		this.toggleComplete = this.toggleComplete.bind(this);
	}

	removeNode (e) {
		e.preventDefault();
		this.props.removeNode(this.props.nodeId);
		return;
	}

	toggleComplete (e) {
		e.preventDefault();
		this.props.toggleComplete(this.props.nodeId);
		return;
	}

	updateClass () {
		
	}

	render() {
		var classes = 'list-group-item clearfix';
		var checkboxButtonClass = 'btn btn-xs change-todo-status-button';
		if (this.props.complete) {
			classes = classes + ' list-group-item-success';
			checkboxButtonClass = checkboxButtonClass + ' completed';
		}
		return (
			<li className={classes}>
				<div className="pull-left" role="group">
					<button type="button" className={checkboxButtonClass} onClick={this.toggleComplete}>&#x2713;</button>
				</div>
				<span>{this.props.task}</span>
				<div className="pull-right" role="group">
					<button type="button" className="btn btn-xs btn-danger img-circle remove-todo-button" onClick={this.removeNode}>&#xff38;</button>
				</div>
			</li>
		);
	}
}