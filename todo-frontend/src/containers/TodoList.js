import React, { Component } from "react";
import TodoItem from './TodoItem';

export default class TodoList extends Component{

	constructor(props){
		super(props);

		this.removeNode = this.removeNode.bind(this);
		this.toggleComplete = this.toggleComplete.bind(this);
	}

	removeNode (nodeId) {
		this.props.removeNode(nodeId);
		return;
	}
	
	toggleComplete (nodeId) {
		this.props.toggleComplete(nodeId);
		return;
	}

	render() {
		var listNodes = this.props.data.map(function (listItem) {
			return (
				<TodoItem key={listItem._id} nodeId={listItem._id} task={listItem.text} complete={listItem.completed} removeNode={this.removeNode} toggleComplete={this.toggleComplete} />
			);
		},this);
		return (
			<ul className="list-group">
				{listNodes}
			</ul>
		);
	}
}