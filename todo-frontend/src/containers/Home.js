import React, { Component } from "react";
import "./Home.css";
import TodoApp from './TodoApp';

export default class Home extends Component {

  constructor(props) {
    super(props);

    if(!this.props.isAuthenticated){
      this.props.history.push('/login');
    }
  }

  render() {
    return (
      <div className="Home">
        <TodoApp />
      </div>
    );
  }
}