# Todo-Levr Project
A simple React Todo App for Levr Full Stack Developer Challenge.

The project contains two folders:

- [**todo-api**](https://gitlab.com/romit/todo-levr/tree/master/todo-api) : Rest API for the frontend using FeathersJS, Express and MongoDB
- [**todo-frontend**](https://gitlab.com/romit/todo-levr/tree/master/todo-frontend) : React App for user login, signup and todo page, used [axios](https://github.com/axios/axios) for api consumption

#### Get project code

- Run following command in your terminal : *git clone https://gitlab.com/romit/todo-levr.git*
- Move to /todo-levr dir

#### How to run

##### Using Docker

The root dir contains *docker-compose.yml* file, which installs all the necessary packages to run this project.
To run this project using docker, [install docker community edition](https://www.docker.com/get-docker), if you don't have it installed already.

- Once you have docker installed on your system, open terminal and run *cd /path/to/todo-levr*.
- In the root dir (/todo-levr) run *docker-compose up*
- Once it installs all the packages and starts the server, open [localhost:3000/signup](http://localhost:3000/signup) in your browser
- To access the API documentation (Swagger), open [localhost:3030/docs](http://localhost:3030/docs)

##### Using npm

For this, you need to have **nodejs** and **npm** installed on you system. You also need **mongodb** to run this project.

- To install nodejs and npm, visit [https://www.npmjs.com/get-npm](https://www.npmjs.com/get-npm)
- To install mongodb, visit [https://docs.mongodb.com/manual/installation/](https://docs.mongodb.com/manual/installation/)

Once you have nodejs, npm and mongodb ready on your system, do following:

- move to */path/to/todo-levr* inside your terminal, now this is your root dir

##### Start the server

- In root dir, run **cd todo-api**
- Inside todo-api dir, run **npm install**
- After installing packages, inside todo-api dir, run **npm start**. This will start the server, which can be accessed at [localhost:3030](http://localhost:3030)
- The docs for the api can be accessed at [localhost:3030/docs](http://localhost:3030/docs)

##### Start the frontend

- In root dir, run **cd todo-frontend**
- Inside todo-frontend dir, run **npm install**
- After installing packages, inside todo-frontend dir, run **npm start**. This will start the website, which can be accessed at [localhost:3000](http://localhost:3000)


### For any help regarding the project and it's installation contact me at [choudharyromit@gmail.com](mailto:choudharyromit@gmail.com)


# General Questions

##### Which point / part was the most difficult to solve / implement for the levr-todo project? Why? And which one the easiest one? Why?

- **The most difficult part** to solve was creating the frontend using React. Since I didn't have much experience with React, getting used to it took some time. The threshold to get started was a bit more than other components.

- **The easiest parts** were integrating docker and swagger to the project. The task were so simple, that they were finished in few minutes. The documentation was also pretty straightforward.

##### If you have to choose other  stack  to implement the todo-levr project, which one will be and why?

I don't have much experience with other stacks as such. So, can't really compare. Also, I liked the feathersjs module, it is really useful for developing quick prototypes.

##### If you are  managing a project and the client has already an existing team of junior PHP developers with a half backed project, asking for help to finish or migrate to a new technology, considering that will take the same amount of time doing both things (finishing in PHP with the team or training + implementing it in a new stack), what will you recommend to the client and why?

That depends on the client's requirements and product use cases. If the technology/framework they're currently using compliments with the requirements, then I'd suggest finishing the project with the current stack only.

But if the technology/framework are outdated and doesn't really go with the client's requirements, then I'd suggest using new tech stack which gives the client more benefits over the old one.

For Example, If the client's requirements are met using the PHP tech stack, and the stack is equivalent in terms of performance and other features, then there will be no point rewriting the whole stack.

##### Let’s say tech is not a choice, what else would you like to do to make a living and why? For example... Artist because i'm extremely creative and appreciate beauty, etc...

Well, if tech is not a choice, I might go for any of the following:

- **High school Teacher** - Because I'd love to guide small kids and students into new ways of learning, and all what I've learned in life.
- **Travel Blogger/Guide or anything related to travel** - I'm always excited with the idea of exploring new places and learning about the history/culture about origins of different places. I've already started exploring different places in India as of now.
- **Experience/product Designer** -  This is something which I might pursue in the near future. I'm always keen to read about design and psychology and how to make products/experiences which makes lives easier.
- **Acting** - This is one thing that I've never done and would love to explore this as a career option.

Also, I've started playing guitar recently, but I don't think that'd make me any living.
